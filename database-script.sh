#!/usr/bin/env bash
pass=$1
dom=test.example.local
# [apache location vars]
fhtml=/var/www/html
fdom=${fhtml}/${dom}
fa_available=/etc/apache2/sites-available
fa_enabled=/etc/apache2/sites-enabled

if [ ! "$pass" ]; then
    echo "password cannot be empty"
    exit 1
fi

sudo apt-get install -y apache2 php mariadb-server mariadb-client php-mysql zip unzip
wget https://www.mysqltutorial.org/wp-content/uploads/2018/03/mysqlsampledatabase.zip
unzip mysqlsampledatabase.zip; rm mysqlsampledatabase.zip


set_sql_root_pass(){
    sudo mysql -u root << EOF
USE mysql;
ALTER USER 'root'@'localhost' INDENTIFIED BY '$pass';
FLUSH PRIVILEGES;
exit;
EOF
# will say acces denied when script is run again but that is fine
}
print_db(){
# -t prints the output to sdout, -e executes the sql between ""
    mysql -u root -t -e "SOURCE ./mysqlsampledatabase.sql; SELECT firstName,lastName,email FROM employees;" -p${pass} > employees.txt
}
create_apache(){
	# create input domain in html folder, then echo start page information ("Dit is de website voor mydomain.be.")
	sudo mkdir -p $fdom;
    sudo tee ${fdom}/index.html << EOF
<html>
    <body>
        <p> hellow worlds </p>
    </body>
</html>
EOF
    sudo tee ${fdom}/test.php << EOF
<html>
<body>
     <p>Today is <?php echo date('D, M d, Y H:i:s'); ?></p>
     <?php
        \$servername = "localhost";
        \$username = "root";
        \$password = "$pass";
        \$database = "classicmodels";
        // Create connection
        \$conn = new mysqli(\$servername, \$username, \$password, \$database);
        \$sqlcmd = "SELECT customerNumber, customerName FROM customers";
        // Check connection
        if (\$conn->connect_error) {
          die("Connection failed: " . \$conn->connect_error);
        }

        echo " nmr | naam <br>";
        echo "+----+-----------<br>";
        \$result = \$conn->query(\$sqlcmd);

        if (\$result->num_rows > 0) {
          // output data of each row
          while(\$row = \$result->fetch_assoc()) {
            echo "" . \$row["customerNumber"]. " | " . \$row["customerName"]. "<br>";
          }
        } else {
          echo "0 results";
        }
        \$conn->close();
    ?>
</body>
EOF
# the above was inspired by the page in the slides (which did not work) and changed with the help of: https://www.w3schools.com/php/php_mysql_select.asp

    sudo tee ${fa_available}/${dom}.conf << EOF
<VirtualHost *:80>
    ServerAdmin admin@example.net
    DocumentRoot /var/www/html/$dom
    ServerName $dom
    ServerAlias www.$dom
    ErrorLog \${APACHE_LOG_DIR}/error.log
    CustomLog \${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
EOF

	# modern website should probably redirect all http traffic to https, like in https://linuxize.com/post/redirect-http-to-https-in-apache/, wont do it here cuz testing
}
enable_apache(){
	# enable site with ln
	sudo ln -s ${fa_available}/${dom}.conf ${fa_enabled}/${dom}.conf
	sudo systemctl restart apache2.service
}

main(){
    set_sql_root_pass
    print_db
    create_apache
    enable_apache
}

main
